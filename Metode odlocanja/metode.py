import numpy as np
import pandas as pd
from pandas import Index

data = pd.read_csv('osnovne_metode.csv',sep = ",",index_col='Izidi')

def hurwitcz():
    x = []
    velikost = len(data.iloc[0])
    i = 0
    h = 0

    while i <= 10:
        j = 0
        row = []
        row.append(round(h,1))
        while j < velikost:
            row.append(round((h * data.iloc[1,j] + (1 - h) * data.iloc[0,j]),2))
            j += 1
        h += 0.1
        i += 1
        x.append(row)
        xi = np.array(x)
    return xi

def optimist():
    maxi = max(data.iloc[1])
    vrni =(data.iloc[1][data.iloc[1]==maxi])
    #vrni1=vrni.name
    return vrni

def pesimist():
    mini = max(data.iloc[0])
    vrni = (data.iloc[0][data.iloc[0] == mini])
    return vrni

def laPlace():
     povprecje = data.mean()
     maxi=max(povprecje)
     vrni = povprecje[povprecje==maxi]
     return vrni
""""
def nekja(s,num):
    tmp=s.sort_values(ascending=False)[:num]
    tmp.index=range(num)
    return tmp
print(data.apply(lambda x: nekja(x,1)))
"""

def najmanjseObzalovanje():
    ar=[]
    a = max(data.iloc[1])
    a1 = (a - data.iloc[1])

    b = max(data.iloc[0])
    b1 = (b - data.iloc[0])
    for i in range(0,len(a1)):
           maxi=max(a1.iloc[i],b1.iloc[i])
           ar.append(maxi)
           xi = np.array(ar)

    mini=min(xi)
    return mini

def vrniRezultate():

     a=optimist()
     b=pesimist()
     c=laPlace()
     d=najmanjseObzalovanje()
     rezultati = [
          ["Optimist:",a],
          ["Pesimist:",b],
          ["Laplace:",c],
          ["Najmanjse obzalovanje:",d]
]
     return rezultati

