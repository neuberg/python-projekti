import matplotlib.pyplot as plt
import pandas as pd
import metode

graf_array = metode.hurwitcz()
graf=pd.DataFrame(data=graf_array)

graf.rename(columns={0:"h"}, inplace=True)
imena=metode.data
for i in range(0,len(graf.iloc[0])-1):
 izpisi=imena.iloc[:,i]
 izpisi1=izpisi.name
 graf.rename(columns={i+1:izpisi1}, inplace=True)

plt.title('Vrednotenje s Hurwiczevim kriterijem')
for x in graf:
 if(x != 'h'):
  plt.plot(graf[x],label=x,marker='x')
print(graf)
plt.xlabel('h(od 0.0 do 1)')

plt.ylabel('vrednost alternativ')
plt.grid(True)

plt.legend(loc='upper center')
plt.show()


