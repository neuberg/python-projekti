import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import main

data=main.data
vrednosti=main.vrednosti()

df = pd.DataFrame({'name':data.columns, 'val':vrednosti})
print(df)



index = np.arange(len(df['name']))
plt.figure()
plt.title("Stolpčni diagram")
for name in df.name.unique():
    tmp_df = df[df.name == name]
    plt.bar(tmp_df.index, tmp_df.val, label=name)
plt.xticks(index, df['name'])

plt.show()