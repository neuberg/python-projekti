# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'gui.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!
import itertools

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QMessageBox
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt




class Ui_MainWindow(object):
    parametri = [""]
    parametri1 = []
    par = []
    vrednostipar = [""]
    vrednostipar1 = []
    vre = []

    alternativa = [""]
    alternativa1 = []
    alt = []
    vrednostialt = [""]
    vrednostialt1 = []
    alter = []

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(805, 560)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.par_frame = QtWidgets.QFrame(self.centralwidget)
        self.par_frame.setGeometry(QtCore.QRect(10, 40, 411, 211))
        self.par_frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.par_frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.par_frame.setObjectName("par_frame")
        self.label = QtWidgets.QLabel(self.par_frame)
        self.label.setGeometry(QtCore.QRect(20, 0, 111, 41))
        self.label.setObjectName("label")
        self.vnos_par = QtWidgets.QLineEdit(self.par_frame)
        self.vnos_par.setGeometry(QtCore.QRect(10, 40, 113, 20))
        self.vnos_par.setObjectName("vnos_par")
        self.vrednost_par = QtWidgets.QSpinBox(self.par_frame)
        self.vrednost_par.setGeometry(QtCore.QRect(150, 40, 42, 22))
        self.vrednost_par.setObjectName("vrednost_par")
        self.label_2 = QtWidgets.QLabel(self.par_frame)
        self.label_2.setGeometry(QtCore.QRect(140, 10, 71, 20))
        self.label_2.setObjectName("label_2")
        self.dodaj_par = QtWidgets.QPushButton(self.par_frame)
        self.dodaj_par.setGeometry(QtCore.QRect(260, 40, 75, 23))
        self.dodaj_par.setObjectName("dodaj_par")
        self.line_2 = QtWidgets.QFrame(self.par_frame)
        self.line_2.setGeometry(QtCore.QRect(210, 0, 20, 211))
        self.line_2.setFrameShape(QtWidgets.QFrame.VLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.line_7 = QtWidgets.QFrame(self.par_frame)
        self.line_7.setGeometry(QtCore.QRect(-3, 210, 411, 20))
        self.line_7.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_7.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_7.setObjectName("line_7")
        self.line_8 = QtWidgets.QFrame(self.par_frame)
        self.line_8.setGeometry(QtCore.QRect(-3, -11, 411, 31))
        self.line_8.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_8.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_8.setObjectName("line_8")
        self.line_9 = QtWidgets.QFrame(self.par_frame)
        self.line_9.setGeometry(QtCore.QRect(400, 10, 20, 201))
        self.line_9.setFrameShape(QtWidgets.QFrame.VLine)
        self.line_9.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_9.setObjectName("line_9")
        self.line_10 = QtWidgets.QFrame(self.par_frame)
        self.line_10.setGeometry(QtCore.QRect(-11, 0, 31, 211))
        self.line_10.setFrameShape(QtWidgets.QFrame.VLine)
        self.line_10.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_10.setObjectName("line_10")
        self.alt_frame = QtWidgets.QFrame(self.centralwidget)
        self.alt_frame.setGeometry(QtCore.QRect(10, 290, 411, 211))
        self.alt_frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.alt_frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.alt_frame.setObjectName("alt_frame")
        self.vnos_alt = QtWidgets.QLineEdit(self.alt_frame)
        self.vnos_alt.setGeometry(QtCore.QRect(10, 60, 121, 20))
        self.vnos_alt.setObjectName("vnos_alt")
        self.label_3 = QtWidgets.QLabel(self.alt_frame)
        self.label_3.setGeometry(QtCore.QRect(10, 30, 131, 16))
        self.label_3.setObjectName("label_3")
        self.label_4 = QtWidgets.QLabel(self.alt_frame)
        self.label_4.setGeometry(QtCore.QRect(10, 120, 191, 20))
        self.label_4.setObjectName("label_4")
        self.dodaj_alt = QtWidgets.QPushButton(self.alt_frame)
        self.dodaj_alt.setGeometry(QtCore.QRect(240, 90, 111, 23))
        self.dodaj_alt.setObjectName("dodaj_alt")
        self.line = QtWidgets.QFrame(self.alt_frame)
        self.line.setGeometry(QtCore.QRect(210, 0, 16, 211))
        self.line.setFrameShape(QtWidgets.QFrame.VLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.vrednost_alt = QtWidgets.QLineEdit(self.alt_frame)
        self.vrednost_alt.setGeometry(QtCore.QRect(10, 150, 121, 20))
        self.vrednost_alt.setObjectName("vrednost_alt")
        self.label_7 = QtWidgets.QLabel(self.alt_frame)
        self.label_7.setGeometry(QtCore.QRect(20, 100, 111, 16))
        self.label_7.setText("")
        self.label_7.setObjectName("label_7")
        self.line_12 = QtWidgets.QFrame(self.alt_frame)
        self.line_12.setGeometry(QtCore.QRect(-11, 0, 31, 211))
        self.line_12.setFrameShape(QtWidgets.QFrame.VLine)
        self.line_12.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_12.setObjectName("line_12")
        self.line_14 = QtWidgets.QFrame(self.alt_frame)
        self.line_14.setGeometry(QtCore.QRect(0, -5, 411, 16))
        self.line_14.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_14.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_14.setObjectName("line_14")
        self.prikazi_vse_rezultate = QtWidgets.QPushButton(self.centralwidget)
        self.prikazi_vse_rezultate.setGeometry(QtCore.QRect(490, 50, 171, 41))
        self.prikazi_vse_rezultate.setObjectName("prikazi_vse_rezultate")
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setGeometry(QtCore.QRect(10, 20, 311, 20))
        self.label_5.setObjectName("label_5")
        self.label_6 = QtWidgets.QLabel(self.centralwidget)
        self.label_6.setGeometry(QtCore.QRect(10, 260, 241, 16))
        self.label_6.setObjectName("label_6")
        self.line_3 = QtWidgets.QFrame(self.centralwidget)
        self.line_3.setGeometry(QtCore.QRect(480, 90, 191, 20))
        self.line_3.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_3.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_3.setObjectName("line_3")
        self.line_4 = QtWidgets.QFrame(self.centralwidget)
        self.line_4.setGeometry(QtCore.QRect(480, 29, 191, 31))
        self.line_4.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_4.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_4.setObjectName("line_4")
        self.line_5 = QtWidgets.QFrame(self.centralwidget)
        self.line_5.setGeometry(QtCore.QRect(470, 40, 20, 61))
        self.line_5.setFrameShape(QtWidgets.QFrame.VLine)
        self.line_5.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_5.setObjectName("line_5")
        self.line_6 = QtWidgets.QFrame(self.centralwidget)
        self.line_6.setGeometry(QtCore.QRect(653, 40, 31, 61))
        self.line_6.setFrameShape(QtWidgets.QFrame.VLine)
        self.line_6.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_6.setObjectName("line_6")
        self.line_11 = QtWidgets.QFrame(self.centralwidget)
        self.line_11.setGeometry(QtCore.QRect(10, 495, 411, 21))
        self.line_11.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_11.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_11.setObjectName("line_11")
        self.line_13 = QtWidgets.QFrame(self.centralwidget)
        self.line_13.setGeometry(QtCore.QRect(415, 290, 21, 211))
        self.line_13.setFrameShape(QtWidgets.QFrame.VLine)
        self.line_13.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_13.setObjectName("line_13")
        self.line_15 = QtWidgets.QFrame(self.centralwidget)
        self.line_15.setGeometry(QtCore.QRect(10, 235, 411, 31))
        self.line_15.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_15.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_15.setObjectName("line_15")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 805, 20))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        self.dodaj_par.clicked.connect(self.dodajParametre)
        self.prikazi_vse_rezultate.clicked.connect(self.glavni)
        self.dodaj_alt.clicked.connect(self.alternativnaVrednost)

    def praznoPoljeWarning(self):
        msg = QMessageBox()
        msg.setWindowTitle("Prazno polje")
        msg.setText("Vsaj eno zahtevano polje je prazno!")
        x = msg.exec_()

    def dodajParametre(self):
        if self.vnos_par.text() == "":
            self.praznoPoljeWarning()
        else:
            self.parametri[0] = self.vnos_par.text()
            self.parametri1.append(self.parametri)

            self.parametri = [""]
            for x in self.par:
                self.parametri.append(0)

            self.vrednostipar[0] = self.vrednost_par.value()
            self.vrednostipar1.append(self.vrednostipar)

            self.vrednostipar = [""]
            for x in self.vre:
                self.vrednostipar.append(0)

            self.vnos_par.setText("")
            self.vrednost_par.setValue(0)

    def alternativnaVrednost(self):
        if self.vnos_alt.text() == "":
            self.praznoPoljeWarning()
        else:
            self.alternativa[0] = self.vnos_alt.text()
            self.alternativa1.append(self.alternativa)

            self.alternativa = [""]
            for x in self.alt:
                self.alternativa.append(0)

            self.vrednostialt[0]=self.vrednost_alt.text()
            self.vrednostialt1.append(self.vrednostialt)

            self.vrednostialt = [""]
            for x in self.alter:
                self.vrednostialt.append(0)

            self.vnos_alt.setText("")
            self.vrednost_alt.setText("")

    def glavni(self):

        matrika=self.vrednostialt1
        seznam=matrika
        x=[[int(i) for i in j[0].split(',')] for j in seznam]

        utezi = self.vrednostipar1
        vrednosti = self.parametri1
        indeksi = self.alternativa1

        utezi_object = itertools.chain.from_iterable(utezi)
        utezi_ar = list(utezi_object)

        vrednosti_object = itertools.chain.from_iterable(vrednosti)
        vrednosti_ar = list(vrednosti_object)

        indeksi_object = itertools.chain.from_iterable(indeksi)
        indeksi_ar = list(indeksi_object)

        data = pd.DataFrame(x, index=indeksi_ar, columns=vrednosti_ar)

        row, col = data.shape
        ar = []
        for j in range(0, row):
            aa = []
            for i in range(0, len(utezi_ar)):
                novo = data.iloc[j, i] * utezi_ar[i]
                aa.append(novo)
            ar.append(aa)
            xi = np.array(ar)

        b = np.sum(xi, axis=1)

        najvecje = max(b)

        lokacija = np.where(b == najvecje)
        print(lokacija)
        lok = lokacija[0]
        lok = lok[0]
        a = data.index.values[lok]



        data1 = data.T

        df = pd.DataFrame({'name': data1.columns, 'val': b})

        index = np.arange(len(df['name']))
        plt.figure()
        plt.title("Stolpčni diagram")
        for name in df.name.unique():
            tmp_df = df[df.name == name]
            plt.bar(tmp_df.index, tmp_df.val, label=name)
        plt.xticks(index, df['name'])
        plt.show()


        df = pd.DataFrame({'name': data1.columns, 'val': b})
        plot = df.plot.pie(y='val', labels=data.index)
        plt.title("Tortni diagram")
        plt.show()

        print(utezi_ar)
        print("Tabela brez izračuna:")
        print(data1)
        print("izračini:")
        print(b)
        print("Največja vrednost:")
        print(najvecje, a)



        """"
        row_labels = vrednosti_ar
        col_labels = indeksi_ar
        ytable = plt.table(cellText=matrika, rowLabels=row_labels, colLabels=col_labels, loc="center right")
        plt.axis("off")
        plt.grid(False)
        plt.show()
        """

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label.setText(_translate("MainWindow", "Vnesite parametre:"))
        self.label_2.setText(_translate("MainWindow", "Določite utež:"))
        self.dodaj_par.setText(_translate("MainWindow", "Dodajte"))
        self.label_3.setText(_translate("MainWindow", "Vnesite naziv alternative:"))
        self.label_4.setText(_translate("MainWindow", "Dodajte vrednost za vsak parameter:"))
        self.dodaj_alt.setText(_translate("MainWindow", "Dodajte alternativo"))
        self.prikazi_vse_rezultate.setText(_translate("MainWindow", "Izračunajte vrednosti"))
        self.label_5.setText(_translate("MainWindow", "Določite vse parametre ter uteži:"))
        self.label_6.setText(_translate("MainWindow", "Določite vse alternative ter vrednosti:"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

