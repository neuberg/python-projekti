import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import main

data=main.data
vrednosti=main.vrednosti()

df = pd.DataFrame({'name':data.columns, 'val':vrednosti})
plot = df.plot.pie(y='val',labels=data.columns)
plt.title("Tortni diagram")
plt.show()