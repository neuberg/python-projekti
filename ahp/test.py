import pandas as pd
import numpy as np


data=[[1,5,3],
      [1/5,1,1/3],
      [1/3,3,1]];
data=pd.DataFrame(data=data)

ar = []
for i in range(len(data)):
    aa = []
    for j in range(len(data)):
        novo = data.iloc[i, j] / sum(data.iloc[:, j])
        aa.append(round(novo,3))
    ar.append(aa)
    xi = np.array(ar)

xi = pd.DataFrame(ar)
x = pd.DataFrame(data=xi)
print("Frame racuna", x)

a = round(x.sum(axis=1)/len(x),3)
print(a)
utez = a.sum(axis=0)
print("Racunanje utezi", utez)