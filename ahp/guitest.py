try:
    import Tkinter
    import ttk
except ImportError:  # Python 3
    import tkinter as Tkinter
    import tkinter.ttk as ttk


class Begueradj(Tkinter.Frame):
    '''
    classdocs
    '''
    def __init__(self, parent):
        '''
        Constructor
        '''
        Tkinter.Frame.__init__(self, parent)


    def initialize_user_interface(self):
        self.root = self.tk.Tk()
        self.root.geometry("1000x500")
        self.root.title("Prikazi")

        self.frame1 = self.tk.LabelFrame(self.root, width=300, height=300, padx=10, pady=10, text="Drevesa:")
        self.frame1.pack(side=self.RIGHT, fill=self.Y)
        self.frame2 = self.tk.LabelFrame(self.root, width=300, height=150, text="Dodaj:")
        self.frame2.pack(side=self.TOP, fill=self.X)
        self.frame3 = self.tk.LabelFrame(self.root, width=300, height=150, text="Vrednosti", )
        self.frame3.pack(side=self.BOTTOM, fill=self.BOTH, expand=1)

        self.i = 0
    def vstavi_drevo(self):
        self.treeview.insert('', 'end', text="Item_" + str(self.i),
                             values=(self.entry1.get() + " mg",))
        # Increment counter
        self.i = self.i + 1

    def insert_data(self):
            self.treeview.insert('', '1', 'item2',
                            text='Computer Science')
            self.treeview.insert('', '2', 'item3',
                            text='GATE papers')
            self.treeview.insert('', 'end', 'item4',
                            text='Programming Languages')

            self.treeview.insert('item2', 'end', 'Algorithm',
                            text='Algorithm')
            self.treeview.insert('item2', 'end', 'Data structure',
                            text='Data structure')
            self.treeview.insert('item3', 'end', '2018 paper',
                            text='2018 paper')
            self.treeview.insert('item3', 'end', '2019 paper',
                            text='2019 paper')
            self.treeview.insert('item4', 'end', 'Python',
                            text='Python')
            self.treeview.insert('item4', 'end', 'Java',
                            text='Java')

            self.button1 = self.tk.Button(self.frame2, text='Dodajte', command=self.vstavi_drevo)
            self.button1.pack()

            self.button1 = self.tk.Button(self.frame2, text='Dodajte otroke', command=self.insert_data)
            self.button1.pack()

            self.label1 = self.tk.Label(self.frame2, text='Vnesite vrednost')
            self.label1.pack()

            self.entry1 = self.tk.Entry(self.frame2)
            self.entry1.pack()

            self.treeview = ttk.Treeview(self.frame1)
            self.treeview.pack()

def main():
    root=Tkinter.Tk()
    d=Begueradj(root)
    root.mainloop()

if __name__=="__main__":
    main()